<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="${contextName}/assets/dist/img/user2-160x160.jpg"
					class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>Admin</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MENU</li>
			<li class="treeview">
				<a href="#"> 
					<i	class="fa fa-dashboard"></i> <span>Master Data</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/supplier.html" class="menu-item"><i class="fa fa-circle-o"></i> Supplier</a></li>
					<li><a href="${contextName}/item.html" class="menu-item"><i class="fa fa-circle-o"></i> Item</a></li>
					<li><a href="${contextName}/servis.html" class="menu-item"><i class="fa fa-circle-o"></i> Servis</a></li>
					<li><a href="${contextName}/distributor.html" class="menu-item"><i class="fa fa-circle-o"></i> Distributor</a></li>
				</ul>
			</li>
			<li>
				<a href="#"> 
					<i	class="fa fa-laptop"></i> <span>Pembayaran</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/penjualan_header.html"><i class="fa fa-circle-o"></i> Penjualan</a></li>
					<li><a href="${contextName}/penjualan_detail.html"><i class="fa fa-circle-o"></i> Detail Penjualan</a></li>
				</ul>
			</li>
			<li>
				<a href="#"> 
					<i	class="fa fa-laptop"></i> <span>Klaim Servis</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/servis_header.html"><i class="fa fa-circle-o"></i> Servis</a></li>
					<li><a href="${contextName}/servis_detail.html"><i class="fa fa-circle-o"></i> Detail</a></li>
				</ul>
			</li>
<%-- 			<li>
				<a href="#"> 
					<i	class="fa fa-pie-chart"></i> <span>Transaksi</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/transaksi/adjustment.html"><i class="fa fa-circle-o"></i> Request</a></li>
					<li><a href="${contextName}/transaksi/transfer.html"><i class="fa fa-circle-o"></i> Order</a></li>
				</ul>
			</li> --%>
			
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>