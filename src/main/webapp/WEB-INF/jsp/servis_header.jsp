<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form id="form-servis" method="post" onsubmit="return validasi();">
<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Form Servis</h3>
	</div>

	<!-- Pengisian Data Pembeli -->
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">Tanggal Servis</label>
			<div class="col-md-6">
				<input type="date" id="tgl_servis" name="tgl_servisStr" class="form-control">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Tanggal Ambil Servis</label>
			<div class="col-md-6">
				<input type="date" id="tgl_ambil_servis" name="tgl_ambil_servisStr" class="form-control">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Pengaju Servis</label>
			<div class="col-md-6">
				<input type="text" id="pengaju_servis" name="pengaju_servis" class="form-control">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Telpon</label>
			<div class="col-md-6">
				<input type="text" id="telpon" name="telpon" class="form-control" maxlength="12" onkeypress="return valTelpon(event)">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Servis</label>
			<div class="col-md-6">
				<div class="box-tools">
					<button type="button" id="btn-add"
						class="btn btn-primary pull-right">
						<i class="fa fa-plus"></i> Tambah
					</button>
				</div>
			</div>
		</div>
	</div>

	<div class="box-body">
		<table class="table" id="tableServisDetail">
			<thead>
				<tr>
					<th>Servis</th>
					<th>Biaya</th>
					<th>Qty</th>
					<th>Sub Total</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody id="list-data">
				<!-- ISI TABLE -->
			</tbody>
		</table>
	</div>
	<br>
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">Total</label>
			<div class="col-md-6">
				<input type="hidden" id="total_jual" name="total_jual" class="form-control">
				<input type="text" id="total_jualdisplay" name="total_jual" class="form-control" disabled="disabled">
				<button type="button" id="btn-total" class="btn btn-primary pull-right" style="margin-top: 5px;" onclick="sumt();">
					<i class="fa fa-dollar"></i> Hasil
				</button>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Tipe Bayar</label>
			<div class="col-md-6">
				<select id="tipe_bayar" name="tipe_bayar" class="form-control" onchange="pilihBayar();">
					<option value="">Pilih</option>
					<option value="Tunai">Tunai</option>
					<option value="Debit">Debit</option>
					<option value="Kredit">Kredit</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Bayar</label>
			<div class="col-md-6">
				<input type="hidden" id="bayar" name="bayar" class="form-control"  onkeyup="sum();" onkeypress="return valBayar(event)">
				<input type="text" id="bayarDisplay" name="bayar" class="form-control" disabled="disabled">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Kembalian</label>
			<div class="col-md-6">
				<input type="hidden" id="kembalian" name="kembalian" class="form-control">
				<input type="text" id="kembalianDisplay" name="kembalianDisplay" class="form-control" disabled="disabled">
			</div>
		</div>
		<br>
		<div class="form-group">
		<label class="control-label col-md-2"></label>
			<div class="col-md-6">
				<div class="box-tools">
					<button type="submit" id="btn-bayar" class="btn btn-primary pull-right" style="display: none;">
						<i class="fa fa-check-square"></i> Proses
					</button>
				</div>
			</div>
		</div>
		<br>
	</div>
</div>
</form>


<!-- Modal (TAMPILAN POPUP) -->
<div id="modal-input" class="modal">
	<!-- class disini adalah nama css, id adalah nama dari div -->
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form Servis</h4>
			</div>
			<div class="modal-body">
				<!-- ISI DI DALAM POPUP -->
			</div>
		</div>
	</div>
</div>


<!-- SCRIPT AJAX  -->
<script type="text/javascript">

	//Validasi
	function validasi () {
		var tgl_servis = document.getElementById("tgl_servis");
		var tgl_ambil_servis = document.getElementById("tgl_ambil_servis");
		var pengaju_servis = document.getElementById("pengaju_servis");
		var telpon = document.getElementById("telpon");
		var bayar = document.getElementById("bayar");
		
		var field = 0;
		var result = false;
		
		if (tgl_servis.value=="") {
			tgl_servis.style.borderColor = "red";
			field++;
		} else {
			tgl_servis.style.borderColor = "white";
		}
		
		if (tgl_ambil_servis.value=="") {
			tgl_ambil_servis.style.borderColor = "red";
			field++;
		} else {
			tgl_ambil_servis.style.borderColor = "white";
		}
		
		if (pengaju_servis.value=="") {
			pengaju_servis.style.borderColor = "red";
			field++;
		} else {
			pengaju_servis.style.borderColor = "white";
		}
		
		if (telpon.value=="") {
			telpon.style.borderColor = "red";
			field++;
		} else {
			telpon.style.borderColor = "white";
		}
		
		if (bayar.value=="") {
			bayar.style.borderColor = "red";
			field++;
		} else {
			bayar.style.borderColor = "white";
		}
		
		if (field!=0) {
			result = false;
			alert("Harap Isi Field");
		} else {
			result = true;
		}
		
		return result;
	}
	
	// VALIDASI TELPON
	function valTelpon(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
	
	// VALIDASI BAYAR
	function valBayar(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
	
	// VALIDASI QTY
	function valQty(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}


	//PILIH BAYAR
	function pilihBayar() {
		var tipe = $('#tipe_bayar').val();
		var total_jual = $('#total_jual').val();
		var nilaiReset = 0;
		var bayar = document.getElementById("bayar");
		var bayarDisplay = document.getElementById("bayarDisplay");
		var buttonSimpan = document.getElementById("btn-bayar");
		
		if(new String(tipe).valueOf() == new String("").valueOf()){
			buttonSimpan.style.display = "none";
			bayar.type = 'hidden';
			bayarDisplay.style.display = "block";
			
			$('#bayar').val(nilaiReset);
			$('#bayarDisplay').val(nilaiReset);
			$('#kembalian').val(nilaiReset);
			$('#kembalianDisplay').val(nilaiReset);
			
			alert("pilih tipe bayar dahulu");
		}else{
			buttonSimpan.style.display = "block";
			
			if(new String(tipe).valueOf() == new String("Tunai").valueOf()){
				bayar.type = 'text';
				bayarDisplay.style.display = "none";
			}else{
				bayar.type = 'hidden';
				bayarDisplay.style.display = "block";
				$('#bayar').val(total_jual);
				$('#bayarDisplay').val(total_jual);
				$('#kembalian').val(nilaiReset);
				$('#kembalianDisplay').val(nilaiReset);
			}
			 
		}
	}
	
	//transaksi header
	function sum() {
	    var bayar = document.getElementById("bayar").value;
	    var total_jual = document.getElementById("total_jual").value;
	    var result = parseInt(bayar) - parseInt(total_jual);
	    $('#kembalian').val(result);
	    $('#kembalianDisplay').val(result);
	}
	
	//transaksi detail
	function sumx(index) {
	    var hj = $("#hargajual_"+index).val();
	    var qty = $("#qty_"+index).val();
	    var result = hj * qty;
	    $("#hasil_"+index).val(result);
	    $("#hasildisplay_"+index).val(result); 
	}
	
	
	//transaksi total bayar
	function sumt() {
	      var jD = $("#jumlahDetail").val();
	      var total =0;
	      for (var i = 0; i < jD; i++) {
	    	  var x = $("#hasil_"+i).val();
			total = parseInt(total) + parseInt(x);
		}
	    $("#total_jual").val(total);
	    $("#total_jualdisplay").val(total);
	} 
	
	
	// Hapus Data Di list
	  	function namex(id) {
			var idx = id;
			console.log(idx);
			$.ajax({
				url:'servis_header/hapus.json',
				type:'post',
				data: { idx : idx},
				
				success:function(data){
				
					if(data.success){
	
						$('#tableServisDetail').empty();
						$('#tableServisDetail').append(' '
								+' <tr> ' 
						        +'   <th>Servis</th> '
						        +'   <th>Biaya</th> '
						        +'   <th>Qty</th> '
						        +'   <th>Sub Total</th> '
						        +'   <th>Aksi</th> ' 
						        +' </tr> ');
						$.each(data.servisDetailList, function(index,jdt){
							$('#tableServisDetail').append(' ' 
									+' <tr> ' 
									+'   <input type="hidden" id="keteranganDetail_'+index+'" name="keteranganDetail_'+index+'" value="'+jdt.servis.keterangan+'"/> '
									+'   <td><input type="hidden"  id="jumlahDetail" name="jumlahDetail" value="'+data.servisDetailList.length+'"/>'+jdt.servis.nama_servis+'</td> '
									+'   <input type="hidden" id="namaServisDetail_'+index+'" name="namaServisDetail_'+index+'" value="'+jdt.servis.nama_servis+'"/> '
									+'   <input type="hidden" id="idServisDetail_'+index+'" name="idServisDetail_'+index+'" value="'+jdt.servis.id_servis+'"/> '
							        +'   <td><input type="hidden" id="hargajual_'+index+'" name="hargajualDetail_'+index+'" value="'+jdt.servis.biaya_servis+'"/>'+jdt.servis.biaya_servis+'</td> '
							        +'   <td><input type="text"  id="qty_'+index+'" name="qtyDetail_'+index+'" style="width: 30px;" onkeyup="sumx('+index+');" onkeypress="return valQty(event)"/></td> '
							        +'   <td><input type="hidden"  id="hasil_'+index+'" name="hasilDetail_'+index+'" /><input type="text" id="hasildisplay_'+index+'" name="hasilDetail_'+index+'" disabled="disabled"/></td> '
							        +'   <td><button type="button" class="btn btn-danger btn-xs btn-delete" onclick="namex(\''+jdt.servis.id_servis+'\');"><i class="fa fa-trash"></i></button></td> '
							        +' </tr> ');
						});
					}	
				}
			});
			
		} 
 
	// EKSEKUSI SELURUH BUTTON/ADD
	$(document).ready(
			function() {
				
				//button cetak di klik
				$("#form-servis").submit(function(){
					$.ajax({
						url:'servis_header/simpan.json',
						type:'post',
						data:$(this).serialize(),  //SERIALIZE : UNTUK MENGAMBIL SELURUH NILAI YG DIINPUT PADA FORM
						dataType:'json',
						success:function(data){
							alert("data berhasil disimpan");
						}
					});
				});
				
				// ini untuk menampilkan popup tambah data
				$("#btn-add").on("click", function() {
					$.ajax({
						url : 'servis_header/add.html',
						type : 'get',
						dataType : 'html',
						success : function(data) {
							$("#modal-input").find(".modal-body").html(data);
							$("#modal-input").modal('show'); // TAMPILKAN POPUP tambah data
						}
					});

				});
				
				 $("#modal-input").on("submit","#form-servis",function(){
					var listItemPilih = new Array();
					$('input[name="listItemPilih"]:checked').each(function() {
								listItemPilih.push(this.value);
							});
					var itemPilih = JSON.stringify(listItemPilih);
					console.log(itemPilih);
					$.ajax({
						url:'servis_header/pilih_detail.json',
						type:'post',
						data: { itemPilih : itemPilih},
						dataType:'json',
						success:function(data){
						
							if(data.success){
								$("#modal-input").modal('hide');
								$('#tableServisDetail').empty();
								$('#tableServisDetail').append(' '
										+' <tr> ' 
								        +'   <th>Servis</th> '
								        +'   <th>Biaya</th> '
								        +'   <th>Qty</th> '
								        +'   <th>Sub Total</th> '
								        +'   <th>Aksi</th> ' 
								        +' </tr> ');
								$.each(data.servisDetailList, function(index,jdt){
									$('#tableServisDetail').append(' ' 
											+' <tr> ' 
											+'   <input type="hidden" id="keteranganDetail_'+index+'" name="keteranganDetail_'+index+'" value="'+jdt.servis.keterangan+'"/> '
											+'   <td><input type="hidden"  id="jumlahDetail" name="jumlahDetail" value="'+data.servisDetailList.length+'"/>'+jdt.servis.nama_servis+'</td> '
											+'   <input type="hidden" id="namaServisDetail_'+index+'" name="namaServisDetail_'+index+'" value="'+jdt.servis.nama_servis+'"/> '
											+'   <input type="hidden" id="idServisDetail_'+index+'" name="idServisDetail_'+index+'" value="'+jdt.servis.id_servis+'"/> '
									        +'   <td><input type="hidden" id="hargajual_'+index+'" name="hargajualDetail_'+index+'" value="'+jdt.servis.biaya_servis+'"/>'+jdt.servis.biaya_servis+'</td> '
									        +'   <td><input type="text"  id="qty_'+index+'" name="qtyDetail_'+index+'" style="width: 30px;" onkeyup="sumx('+index+');" onkeypress="return valQty(event)"/></td> '
									        +'   <td><input type="hidden"  id="hasil_'+index+'" name="hasilDetail_'+index+'" /><input type="text" id="hasildisplay_'+index+'" name="hasilDetail_'+index+'" disabled="disabled"/></td> '
									        +'   <td><button type="button" class="btn btn-danger btn-xs btn-delete" onclick="namex(\''+jdt.servis.id_servis+'\');"><i class="fa fa-trash"></i></button></td> '
									        +' </tr> ');
								});
							}	
						}
					});
					return false;
				}); 
			}); 
</script>