<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form id="form-servis" method="post">
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>Nama ServisDD</th>
					<th>Biaya</th>
					<th>Pilih</th>
				</tr>
			</thead>
			<tbody id="list-data">
				<c:choose>
					<c:when test="${listServis.size()>0}">
						<c:forEach var="servis" items="${listServis}">
							<tr>
								<td>${servis.nama_servis}</td>
								<td>Rp. ${servis.biaya_servis}</td>
								<td><input type="checkbox" name="listItemPilih" value="${servis.id_servis}"/></td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr>
							<td colspan="4">Servis Kosong</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	
	<c:choose>
		<c:when test="${listServis.size()>0}">
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Simpan</button>
			</div>
		</c:when>


	</c:choose>
</form>

