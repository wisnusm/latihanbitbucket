<form id="form-supplier" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id_supplier" name="id_supplier" class="form-control" required="required">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Supplier</label>
			<div class="col-md-6">
				<input type="text" id="nama_supplier" name="nama_supplier" class="form-control" required="required">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Alamat</label>
			<div class="col-md-6">
				<textarea id="alamat" name="alamat" class="form-control" required="required" form="form-supplier"></textarea>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">No. Kontak</label>
			<div class="col-md-6">
				<input type="tel" size="20" pattern="[0-9]{9,14}" minlength="9" maxlength="14" id="nomor_hp" name="nomor_hp" class="form-control" required="required">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Email</label>
			<div class="col-md-6">
				<input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" id="email" name="email" class="form-control" required="required">
			</div>					
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>

<script>
$("#id_supplier").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});

$("#nama_supplier").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});

$("#alamat").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});

$("#nomor_hp").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});

$("#email").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});
</script>