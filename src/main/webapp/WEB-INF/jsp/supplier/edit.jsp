<form id="form-supplier" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="ud_id_supplier" name="id_supplier" class="form-control" value="${item.id_supplier }" disabled="disabled">
				<input type="hidden" id="id_supplier" name="id_supplier" class="form-control" value="${item.id_supplier }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Supplier</label>
			<div class="col-md-6">
				<input type="text" required="required" id="nama_supplier" name="nama_supplier" class="form-control" value="${item.nama_supplier }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Alamat</label>
			<div class="col-md-6">
				<textarea id="alamat" name="alamat" class="form-control" required="required" form="form-supplier">${item.alamat }</textarea>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">No. Kontak</label>
			<div class="col-md-6">
				<input type="tel" required="required" size="20" pattern="[0-9]{9,14}" minlength="9" maxlength="14" id="nomor_hp" name="nomor_hp" class="form-control" value="${item.nomor_hp }">
			</div>					
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Email</label>
			<div class="col-md-6">
				<input type="email" required="required" id="email" name="email" class="form-control" value="${item.email }">
			</div>					
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>