<form id="form-supplier" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="text" id="id_supplier" name="id_supplier" class="form-control" value="${item.id_supplier }" disabled="disabled">
		<input type="text" id="nama_supplier" name="nama_supplier" class="form-control" value="${item.nama_supplier }" disabled="disabled">
		<input type="text" id="alamat" name="alamat" class="form-control" value="${item.alamat }" disabled="disabled">
		<input type="text" id="nomor_hp" name="nomor_hp" class="form-control" value="${item.nomor_hp }" disabled="disabled">
				<input type="text" id="email" name="email" class="form-control" value="${item.email }" disabled="disabled">
		
		<div class="form-group">
			<p>Apakah anda yakin mau menghapus data supplier ${item.nama_supplier} ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
