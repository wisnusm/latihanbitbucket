<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Supplier</title>
</head>
<body>


<div class="box">
            <div class="box-header">
              <h3 class="box-title">Detail Supplier</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						ID
					</td>
					<td>
						${item.id_supplier }
					</td>
				</tr>
				<tr>
					<td>
						Supplier
					</td>
					<td>
						${item.nama_supplier }
					</td>
				</tr>
				<tr>
					<td>
						Alamat
					</td>
					<td>
						${item.alamat }
					</td>
				</tr>
				<tr>
					<td>
						No. Kontak
					</td>
					<td>
						${item.nomor_hp }
					</td>
				</tr>
				<tr>
					<td>
						Email
					</td>
					<td>
						${item.email }
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="supplier.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>