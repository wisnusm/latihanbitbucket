<form id="form-item" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="text" id="id_item" name="id_item" class="form-control" value="${item.id_item }">
		<input type="text" id="Idproduk" name="Idproduk" class="form-control" value="${item.nama_produk }">
		<input type="text" id="nama_item" name="nama_item" class="form-control" value="${item.nama_item }">
		<input type="text" id="harga_jual" name="harga_jual" class="form-control" value="${item.harga_jual }">
		<input type="text" id="stok" name="stok" class="form-control" value="${item.stok }">
		
		<div class="form-group">
			<p>Apakah Anda Yakin Ingin Menghapus Data Item  ${item.id_item} ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
