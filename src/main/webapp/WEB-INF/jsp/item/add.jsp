<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-item" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">ID Item</label>
			<div class="col-md-6">
				<input type="text" required="required" id="id_item" name="id_item" class="form-control">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Produk</label>
			<div class="col-md-6">
				<input type="text" required="required" id="nama_produk"name="nama_produk" class="form-control">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Item</label>
			<div class="col-md-6">
				<input type="text" required="required" id="nama_item"name="nama_item" class="form-control">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Harga Jual</label>
			<div class="col-md-6">
				<input type="number" required="required" id="harga_jual" name="harga_jual" class="form-control">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Stok</label>
			<div class="col-md-6">
				<input type="number" required="required" id="stok" name="stok" class="form-control" min="0" max="9999">
			</div>					
		</div>		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>

<script>
$("#id_item").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});

$("#nama_item").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
});
</script>