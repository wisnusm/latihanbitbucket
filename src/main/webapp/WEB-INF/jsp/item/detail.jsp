<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Item</title>
</head>
<body>


<div class="box">
            <div class="box-header">
              <h3 class="box-title">Detail Distributor</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						ID Item
				</td>
					<td>
						${item.id_item }
					</td>
				</tr>
				<tr>
					<td>
						Nama Produk
					</td>
					<td>
						${item.nama_produk }
					</td>
				</tr>
				<tr>
					<td>
						Nama Item
					</td>
					<td>
						${item.nama_item }
					</td>
				</tr>
				
				<tr>
					<td>
						Harga Jual
					</td>
					<td>
						${item.harga_jual }
					</td>
				</tr>
				
				<tr>
					<td>
						Stok
					</td>
					<td>
						${item.stok }
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="item.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>