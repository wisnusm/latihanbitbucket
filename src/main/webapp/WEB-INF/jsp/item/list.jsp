<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.id_item}</td>
		<td>${item.nama_produk}</td>
		<td>${item.nama_item}</td>
		<td>${item.harga_jual}</td>
		<td>${item.stok}</td>
		<td>
			<button type="button" class="btn btn-success btn-xs btn-edit" value="${item.id_item }"><i class="fa fa-edit"></i></button>
			<button type="button" class="btn btn-danger btn-xs btn-delete" value="${item.id_item }"><i class="fa fa-trash"></i></button>
			<button type="button" class="btn btn-info btn-xs btn-detail" value="${item.id_item }"><i class="fa fa-eye"></i></button>
		</td>
	</tr>
</c:forEach>