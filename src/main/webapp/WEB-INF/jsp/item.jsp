<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Data Item</h3>
		<div class="box-tools">
			<button type="button" id="btn-add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
		</div>
	</div>
	<div class="box-body">
		<table class="table" id="tbl_item">
			<thead>
				<tr>
					<th>ID Item</th>
					<th>Nama Produk</th>
					<th>Nama Item</th>
					<th>Harga Jual</th>
					<th>Stok</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="list-data">
				<!-- ISI TABLE -->
			</tbody>
		</table>
	</div>
</div>

<!-- Modal (TAMPILAN POPUP) -->
<div id="modal-input" class="modal">   <!-- class disini adalah nama css, id adalah nama dari div -->
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form Item</h4>
			</div>
			<div class="modal-body">
				<!-- ISI DI DALAM POPUP -->
			</div>
		</div>
	</div>
</div>


<!-- SCRIPT AJAX  -->
<script>
	
$(function(){
	$("tbl_item").DataTable({
		responsive:true
	});
});

	/* loadData akan mengisi nilai ke dalam tabel list-data diatas */
	function loadData() {
		$.ajax({
			url:'item/list.html', // nama action ke controller (.html hanya tipu2/manipulasi)
			type:'get',
			dataType:'html',
			success:function(data){
				$("#list-data").html(data);
			}
		});
	}
	
	loadData();
	
	// EKSEKUSI SELURUH BUTTON
	$(document).ready(function() {
		
		// ini untuk menampilkan popup tambah data
		$("#btn-add").on("click",function(){
			$.ajax({
				url:'item/add.html',
				type:'get',
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');  // TAMPILKAN POPUP tambah data
				}
			});
			
		});
		
		
		// button edit di klik
		$("#list-data").on("click",".btn-edit",function(){
			var vId = $(this).val();
			$.ajax({
				url:'item/edit.html',
				type:'post',
				data:{ id_item:vId },
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		
		
		// button delete di klik
		$("#list-data").on("click",".btn-delete",function(){
			var vId = $(this).val();
			$.ajax({
				url:'item/delete.html',
				type:'post',
				data:{ id_item:vId },
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		
		$("#list-data").on("click",".btn-detail",function(){
			var vId = $(this).val();
			$.ajax({
				url:'item/detail.html',
				type:'get',
				data:{ id_item:vId },
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
		
		
		//ini untuk save data setelah add/edit/delete
		$("#modal-input").on("submit","#form-item",function(){
			$.ajax({
				url:'item/save.json',
				type:'post',
				data:$(this).serialize(),  //SERIALIZE : SELURUH TEXTFEILD YG MEMILIKI NAME AKAN DIAMBIL
				dataType:'json',
				success:function(data){
					if(data.result=="berhasil"){
						$("#modal-input").modal('hide');
						loadData();
					}
					else{
						alert(data.message);
					}
				}
			});
			return false;
		});
		
	});

</script>