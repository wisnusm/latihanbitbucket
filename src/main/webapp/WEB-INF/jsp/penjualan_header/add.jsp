<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form id="form-penjualan" action="save" method="post">
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>Produk</th>
					<th>Item</th>
					<th>Harga Jual</th>
					<th>Stok</th>
					<th>Pilih</th>
				</tr>
			</thead>
			<tbody id="list-data">
				<c:choose>
					<c:when test="${listItem.size()>0}">
						<c:forEach var="item" items="${listItem}">
							<tr>
								<td>${item.nama_produk}</td>
								<td>${item.nama_item}</td>
								<td>Rp. ${item.harga_jual}</td>
								<td>${item.stok}</td>
								<td><input type="checkbox" name="listItemPilih" value="${item.id_item}"/></td>
							</tr>
					</c:forEach>
				</c:when>
					<c:otherwise>
						<tr>
							<td colspan="4">Servis Kosong</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	
	<c:choose>
		<c:when test="${listItem.size()>0}">
		<div class="modal-footer">
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
		</c:when>
	</c:choose>
</form>





