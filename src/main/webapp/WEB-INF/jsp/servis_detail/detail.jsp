<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Servis</title>
</head>
<body>

<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Transaksi Servis</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						Tanggal Servis
				</td>
					<td>
						${item.tgl_servis }
					</td>
				</tr>
				<tr>
                <td>
						Tanggal Ambil
				</td>
					<td>
						${item.tgl_ambil_servis }
					</td>
				</tr>
                <tr>
                <td>
						Nama Pengaju
				</td>
					<td>
						${item.pengaju_servis }
					</td>
				</tr>
				<tr>
					<td>
						Telpon
					</td>
					<td>
						${item.telpon }
					</td>
				</tr>
				<tr>
					<td>
						Total Bayar
					</td>
					<td>
						Rp. ${item.total }
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
			
 			<div class="form-group">
			<table class="table">
			<thead>
				<tr>
					<th>Nama Servis</th>
					<th>Keterangan</th>
					<th>Biaya</th>
					<th>Qty</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${item.listServisDetail}">
					<tr>
						<td>${item.nama_servis}</td>
						<td>${item.keterangan}</td>
						<td>Rp. ${item.biaya}</td>
						<td>${item.jumlah_stok}</td>
						<td>Rp. ${item.sub_total}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>
	<br>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="penjualan_detail.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>