<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Data Penjualan</h3>
	</div>
	<div class="box-body">
		<table class="table">
			<thead>
				<tr>
					<th>Tanggal</th>
					<th>Nama</th>
					<th>Telpon</th>
					<th>Total</th>
					<th>Tipe Bayar</th>
					<th>Bayar</th>
					<th>Kembalian</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody id="list-data">
				<!-- ISI TABLE -->
			</tbody>
		</table>
	</div>
</div>

<!-- Modal (TAMPILAN POPUP) -->
<div id="modal-input" class="modal">   <!-- class disini adalah nama css, id adalah nama dari div -->
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form Transaksi Penjualan</h4>
			</div>
			<div class="modal-body">
				<!-- ISI DI DALAM POPUP -->
			</div>
		</div>
	</div>
</div>


<!-- SCRIPT AJAX  -->
<script>

	/* loadData akan mengisi nilai ke dalam tabel list-data diatas */
	function loadData() {
		$.ajax({
			url:'penjualan_detail/list.html', // nama action ke controller (.html hanya tipu2/manipulasi)
			type:'get',
			dataType:'html',
			success:function(data){
				$("#list-data").html(data);
			}
		});
	}
	
	loadData();
	
	// EKSEKUSI SELURUH BUTTON
	$(document).ready(function() {
		
		$("#list-data").on("click",".btn-detail",function(){
			var vId = $(this).val();
			$.ajax({
				url:'penjualan_detail/detail.html',
				type:'get',
				data:{ id_jual_hdr:vId },
				dataType:'html',
				success:function(data){
					$("#modal-input").find(".modal-body").html(data);
					$("#modal-input").modal('show');
				}
			});
		});
	});

</script>