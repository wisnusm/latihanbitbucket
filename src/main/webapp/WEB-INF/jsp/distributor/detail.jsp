<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Distributor</title>
</head>
<body>


<div class="box">
            <div class="box-header">
              <h3 class="box-title">Detail Distributor</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						ID Distributor
				</td>
					<td>
						${item.id_distributor }
					</td>
				</tr>
				<tr>
					<td>
						Nama Distributor
					</td>
					<td>
						${item.nama_distributor }
					</td>
				</tr>
				<tr>
					<td>
						Telpon Klaim
					</td>
					<td>
						${item.telpon_klaim }
					</td>
				</tr>
				
				<tr>
					<td>
						Telpon Service Center
					</td>
					<td>
						${item.telpon_sc }
					</td>
				</tr>
				
				<tr>
					<td>
						Alamat Klaim
					</td>
					<td>
						${item.alamat_klaim }
					</td>
				</tr>
				
				<tr>
					<td>
						Alamat Service Center
					</td>
					<td>
						${item.alamat_sc }
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="distributor.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>