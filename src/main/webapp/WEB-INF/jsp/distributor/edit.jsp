<form id="form-distributor" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
		<div class="form-group">
			<label class="control-label col-md-2">ID Distributor</label>
			<div class="col-md-6">
				<input type="text" id="id_distributor" name="id_distributor" class="form-control" value="${item.id_distributor }" disabled="disabled">
				<input type="hidden" id="id_distributor" name="id_distributor" class="form-control" value="${item.id_distributor }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama Distributor</label>
			<div class="col-md-6">
				<input type="text" id="nama_distributor" name="nama_distributor" class="form-control" value="${item.nama_distributor }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Telpon Klaim</label>
			<div class="col-md-6">
				<input type="text" id="telpon_klaim" name="telpon_klaim" class="form-control" value="${item.telpon_klaim }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Telpon Service Center</label>
			<div class="col-md-6">
				<input type="text" id="telpon_sc" name="telpon_sc" class="form-control" value="${item.telpon_sc }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Alamat Klaim</label>
			<div class="col-md-6">
				<input type="text" id="alamat_klaim" name="alamat_klaim" class="form-control" value="${item.alamat_klaim }">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Alamat Service Center</label>
			<div class="col-md-6">
				<input type="text" id="alamat_sc" name="alamat_sc" class="form-control" value="${item.alamat_sc }">
			</div>					
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>