<form id="form-distributor" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="text" id="id_distributor" name="id_distributor" class="form-control" value="${item.id_distributor }"><br>
		<input type="text" id="nama_distributor" name="nama_distributor" class="form-control" value="${item.nama_distributor }"><br>
		<input type="text" id="telpon_klaim" name="telpon_klaim" class="form-control" value="${item.telpon_klaim }"><br>
		<input type="text" id="telpon_sc" name="telpon_sc" class="form-control" value="${item.telpon_sc }"><br>
		<input type="text" id="alamat_klaim" name="alamat_klaim" class="form-control" value="${item.alamat_klaim }"><br>
		<input type="text" id="alamat_sc" name="alamat_sc" class="form-control" value="${item.alamat_sc }"><br>
		
		<div class="form-group">
			<p>Apakah Anda Yakin Ingin Menghapus Data Produk  ${item.nama_distributor} ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
