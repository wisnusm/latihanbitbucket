<form id="form-distributor" action="save" method="post" onsubmit="return validasi();">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id_distributor" name="id_distributor" class="form-control" onblur="cek_id();">
				<div id="a_id" style="display: none;">
					<a style="color : red; font-size : small;">ID Distributor Tidak Boleh Kosong</a>
				</div>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="text" id="nama_distributor" name="nama_distributor" class="form-control" onblur="cek_nama();">
				<div id="a_name" style="display: none;">
					<a style="color : red; font-size : small;">Yakin Nama Distributor Kosong ?</a>
				</div>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Telpon Klaim</label>
			<div class="col-md-6">
				<input type="text" id="telpon_klaim" name="telpon_klaim" class="form-control" onblur="cek_tk();" onkeypress="return valTelponK(event)">
				<div id="a_tk" style="display: none;">
					<a style="color : red; font-size : small;">Yakin Telpon Klaim Kosong ?</a>
				</div>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Telpon SC</label>
			<div class="col-md-6">
				<input type="text" id="telpon_sc" name="telpon_sc" class="form-control" onblur="cek_tsc();" onblur="cek_tk();" onkeypress="return valTelponSC(event)">
				<div id="a_tsc" style="display: none;">
					<a style="color : red; font-size : small;">Yakin Telpon Service Center Kosong ?</a>
				</div>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Alamat Klaim</label>
			<div class="col-md-6">
				<input type="text" id="alamat_klaim" name="alamat_klaim" class="form-control" onblur="cek_ak();">
				<div id="a_ak" style="display: none;">
					<a style="color : red; font-size : small;">Yakin Alamat Klaim Kosong ?</a>
				</div>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Alamat SC</label>
			<div class="col-md-6">
				<input type="text" id="alamat_sc" name="alamat_sc" class="form-control" onblur="cek_asc();">
				<div id="a_asc" style="display: none;">
					<a style="color : red; font-size : small;">Yakin Alamat Service Center Kosong ?</a>
				</div>
			</div>					
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>

<script type="text/javascript">
	function validasi () {
		var id_distributor = document.getElementById("id_distributor");
		var nama_distributor = document.getElementById("nama_distributor");
		var telpon_klaim = document.getElementById("telpon_klaim");
		var telpon_sc = document.getElementById("telpon_sc");
		var alamat_klaim = document.getElementById("alamat_klaim");
		var alamat_sc = document.getElementById("alamat_sc");
		
 		var field = 0;
		var result = false;
		
		if (id_distributor.value=="") {
			id_distributor.style.borderColor = "red";
			field++;
		} else {
			id_distributor.style.borderColor = "white";
		}
		
		if (nama_distributor.value=="") {
			nama_distributor.style.borderColor = "red";
			field++;
		} else {
			nama_distributor.style.borderColor = "white";
		}
		
		if (telpon_klaim.value=="") {
			telpon_klaim.style.borderColor = "red";
			field++;
		} else {
			telpon_klaim.style.borderColor = "white";
		}
		
		if (telpon_sc.value=="") {
			telpon_sc.style.borderColor = "red";
			field++;
		} else {
			telpon_sc.style.borderColor = "white";
		}
		
		if (alamat_klaim.value=="") {
			alamat_klaim.style.borderColor = "red";
			field++;
		} else {
			alamat_klaim.style.borderColor = "white";
		}
		
		if (alamat_sc.value=="") {
			alamat_sc.style.borderColor = "red";
			field++;
		} else {
			alamat_sc.style.borderColor = "white";
		}
		
		if (field!=0) {
			result = false;
			alert("Harap Isi Fieldnya...");
		} else {
			result = true;
		}
		
		return result;
	}
	
	function cek_id() {
		var id_distributor = document.getElementById("id_distributor");
		var a_id = document.getElementById("a_id");
		if (id_distributor.value == "") {
			id_distributor.style.borderColor = "red";
			a_id.style.display = "block";
		} else {
			id_distributor.style.borderColor = "transparent";
			a_id.style.display = "none";
		}
	}
	
	function cek_nama() {
		var nama_distributor = document.getElementById("nama_distributor");
		var a_name = document.getElementById("a_name");
		if (nama_distributor.value == "") {
			nama_distributor.style.borderColor = "red";
			a_name.style.display = "block";
		} else {
			nama_distributor.style.borderColor = "transparent";
			a_name.style.display = "none";
		}
	}
	
	function cek_tk() {
		var telpon_klaim = document.getElementById("telpon_klaim");
		var a_tk = document.getElementById("a_tk");
		if (telpon_klaim.value == "") {
			telpon_klaim.style.borderColor = "red";
			a_tk.style.display = "block";
		} else {
			telpon_klaim.style.borderColor = "transparent";
			a_tk.style.display = "none";
		}
	}
	
	function cek_tsc() {
		var telpon_sc = document.getElementById("telpon_sc");
		var a_tsc = document.getElementById("a_tsc");
		if (telpon_sc.value == "") {
			telpon_sc.style.borderColor = "red";
			a_tsc.style.display = "block";
		} else {
			telpon_sc.style.borderColor = "transparent";
			a_tsc.style.display = "none";
		}
	}
	
	function cek_ak() {
		var alamat_klaim = document.getElementById("alamat_klaim");
		var a_ak = document.getElementById("a_ak");
		if (alamat_klaim.value == "") {
			alamat_klaim.style.borderColor = "red";
			a_ak.style.display = "block";
		} else {
			alamat_klaim.style.borderColor = "transparent";
			a_ak.style.display = "none";
		}
	}
	
	function cek_asc() {
		var alamat_sc = document.getElementById("alamat_sc");
		var a_asc = document.getElementById("a_asc");
		if (alamat_sc.value == "") {
			alamat_sc.style.borderColor = "red";
			a_asc.style.display = "block";
		} else {
			alamat_sc.style.borderColor = "transparent";
			a_asc.style.display = "none";
		}
	}
	
	// VALIDASI TELPON KLAIM
	function valTelponK(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
	
	// VALIDASI TELPON KLAIM
	function valTelponSC(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
</script>