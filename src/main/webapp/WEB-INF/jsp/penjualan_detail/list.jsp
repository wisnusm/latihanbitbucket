<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.tgl_pembelian}</td>
		<td>${item.nama_pembeli}</td>
		<td>${item.telpon}</td>
		<td>${item.total_jual}</td>
		<td>${item.tipe_bayar}</td>
		<td>${item.bayar}</td>
		<td>${item.kembalian}</td>
		<td>
			<button type="button" class="btn btn-info btn-xs btn-detail" value="${item.id_jual_hdr }"><i class="fa fa-eye"></i></button>
		</td>
	</tr>
</c:forEach>