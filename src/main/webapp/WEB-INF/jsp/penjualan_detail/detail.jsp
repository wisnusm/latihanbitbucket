<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Servis</title>
</head>
<body>


<div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Klaim Servis</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						Nama Pengaju
				</td>
					<td>
						${item.nama_pembeli }
					</td>
				</tr>
				<tr>
					<td>
						Telpon
					</td>
					<td>
						${item.telpon }
					</td>
				</tr>
				<tr>
					<td>
						Total Bayar
					</td>
					<td>
						Rp. ${item.total_jual }
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
			
			<div class="form-group">
			<table class="table">
			<thead>
				<tr>
					<th>Produk</th>
					<th>Item</th>
					<th>Harga</th>
					<th>Qty</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${item.jualdetail}">
					<tr>
						<td>${item.nama_produk}</td>
						<td>${item.nama_item}</td>
						<td>Rp. ${item.harga_jual}</td>
						<td>${item.jumlah_stok}</td>
						<td>Rp. ${item.sub_total}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>
	<br>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="penjualan_detail.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>