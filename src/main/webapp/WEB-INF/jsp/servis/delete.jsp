<form id="form-servis" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete"><br>
		<input type="text" id="id_servis" name="id_servis" class="form-control" value="${item.id_servis }"><br>
		<input type="text" id="nama_servis" name="nama_servis" class="form-control" value="${item.nama_servis }"><br>
		<input type="text" id="biaya_servis" name="biaya_servis" class="form-control" value="${item.biaya_servis }"><br>
		<input type="text" id="keterangan" name="keterangan" class="form-control" value="${item.keterangan }"><br>
		
		<div class="form-group">
			<p style="margin-left: 30px;">Apakah Anda Yakin Ingin Menghapus Data Produk  ${item.nama_servis} ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
