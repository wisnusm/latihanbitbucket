<form id="form-servis" action="save" method="post" onsubmit="return validasi();">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id_servis" name="id_servis" class="form-control" onblur="cek_id();">
				<div id="a_id" style="display: none;">
					<a style="color : red; font-size : small;">ID Servis Tidak Boleh Kosong</a>
				</div>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="text" id="nama_servis" name="nama_servis" class="form-control" onblur="cek_nama();">
				<div id="a_name" style="display: none;">
					<a style="color : red; font-size : small;">Yakin Nama Kosong ?</a>
				</div>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Biaya Servis</label>
			<div class="col-md-6">
				<input type="text" id="biaya_servis" name="biaya_servis" class="form-control" onblur="cek_biaya();">
				<div id="a_biaya" style="display: none;">
					<a style="color : red; font-size : small;">Yakin Biaya Kosong ?</a>
				</div>
				<div id="a_num" style="display: none;">
					<a style="color : red; font-size : small;">Masukan Harus Angka</a>
				</div>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan</label>
			<div class="col-md-6">
				<input type="text" id="keterangan" name="keterangan" class="form-control" onblur="cek_keterangan();">
				<div id="a_keterangan" style="display: none;">
					<a style="color : red; font-size : small;">Yakin Keterangan Kosong ?</a>
				</div>
			</div>					
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success" onclick="validasiBiaya()">Simpan</button>
	</div>
</form>

<script type="text/javascript">
	function validasi () {
		var id_servis = document.getElementById("id_servis");
		var nama_servis = document.getElementById("nama_servis");
		var biaya_servis = document.getElementById("biaya_servis");
		var keterangan = document.getElementById("keterangan");
		
 		var field = 0;
		var result = false;
		
		if (id_servis.value=="") {
			id_servis.style.borderColor = "red";
			field++;
		} else {
			id_servis.style.borderColor = "white";
		}
		
		if (nama_servis.value=="") {
			nama_servis.style.borderColor = "red";
			field++;
		} else {
			nama_servis.style.borderColor = "white";
		}
		
		if (biaya_servis.value=="") {
			biaya_servis.style.borderColor = "red";
			field++;
		} else {
			biaya_servis.style.borderColor = "white";
		}
		
		if (keterangan.value=="") {
			keterangan.style.borderColor = "red";
			field++;
		} else {
			keterangan.style.borderColor = "white";
		}
		
		if (field!=0) {
			result = false;
			alert("Harap Isi Field");
		} else {
			result = true;
		}
		
		return result;
	}
	
	function cek_id() {
		var id_servis = document.getElementById("id_servis");
		var a_id = document.getElementById("a_id");
		if (id_servis.value == "") {
			id_servis.style.borderColor = "red";
			a_id.style.display = "block";
		} else {
			id_servis.style.borderColor = "transparent";
			a_id.style.display = "none";
		}
	}
	
	function cek_nama() {
		var nama_servis = document.getElementById("nama_servis");
		var a_name = document.getElementById("a_name");
		if (nama_servis.value == "") {
			nama_servis.style.borderColor = "red";
			a_name.style.display = "block";
		} else {
			nama_servis.style.borderColor = "transparent";
			a_name.style.display = "none";
		}
	}
	
	function cek_biaya() {
		var biaya_servis = document.getElementById("biaya_servis");
		var a_biaya = document.getElementById("a_biaya");
		var number = /^[0-9]+$/;
		var a_num = document.getElementById("a_num");
		if (biaya_servis.value == "") {
			biaya_servis.style.borderColor = "red";
			a_biaya.style.display = "block";			
		} else if (!biaya_servis.value.match(number)) {
			biaya_servis.style.borderColor = "red";
			a_num.style.display = "block"
		} else {
			biaya_servis.style.borderColor = "transparent";
			a_biaya.style.display = "none";
			a_num.style.display = "none"
		}
	}
	
	function cek_keterangan() {
		var keterangan = document.getElementById("keterangan");
		var a_keterangan = document.getElementById("a_keterangan");
		if (keterangan.value == "") {
			keterangan.style.borderColor = "red";
			a_keterangan.style.display = "block";
		} else {
			keterangan.style.borderColor = "transparent";
			a_keterangan.style.display = "none";
		}
	}
	
	function validasiBiaya() {
	var biaya = document.forms["form-servis"]["biaya_servis"].value;
	var number = /^[0-9]+$/;
	var a_num = document.getElementById("a_num");
	
	if (!biaya.match(number)) {
		a_num.style.display = "block"
		return false;
	} else {
		a_num.style.display = "none";
	}
} 
	
</script>