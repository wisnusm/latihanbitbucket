<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Servis</title>
</head>
<body>


<div class="box">
            <div class="box-header">
              <h3 class="box-title">Detail Servis</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                <td>
						ID Servis
				</td>
					<td>
						${item.id_servis }
					</td>
				</tr>
				<tr>
					<td>
						Nama Servis
					</td>
					<td>
						${item.nama_servis }
					</td>
				</tr>
				<tr>
					<td>
						Biaya Servis
					</td>
					<td>
						${item.biaya_servis }
					</td>
				</tr>
				<tr>
					<td>
						Keterangan
					</td>
					<td>
						${item.keterangan }
					</td>
				</tr>
				</tbody>
			  </table>
			</div>
	<table>
	<tr>
		<td colspan="2">
		<!-- Kembali ke list -->
		<a class="btn btn-block btn-danger" href="servis.html">Back</a>
		</td>
	</tr>
</table>
</div>
</body>
</html>