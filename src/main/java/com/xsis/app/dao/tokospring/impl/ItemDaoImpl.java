package com.xsis.app.dao.tokospring.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.app.tokospring.dao.ItemDao;
import com.xsis.app.tokospring.model.ItemModel;

@Repository
public class ItemDaoImpl implements ItemDao{
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<ItemModel> result = session.createQuery("from ItemModel").list();
		return result;
	}

	@Override
	public void insert(ItemModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public ItemModel getById(String id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		ItemModel result = session.get(ItemModel.class, id);
		return result;
	}

	@Override
	public void update(ItemModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(ItemModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemModel> getByIdNotIn(String idExcludeList) throws Exception {
		// TODO Auto-generated method stub
		String condition ="";
		if(!idExcludeList.isEmpty()) {
			condition = "where id NOT IN ("+idExcludeList+" ) ";
		}
		Session session = this.sessionFactory.getCurrentSession();
		List<ItemModel> result = session.createQuery("from ItemModel "+condition).list();
		return result;
	}	
}
