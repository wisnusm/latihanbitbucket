package com.xsis.app.dao.tokospring.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.app.tokospring.dao.DistributorDao;
import com.xsis.app.tokospring.model.DistributorModel;

@Repository
public class DistributorDaoImpl implements DistributorDao{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<DistributorModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<DistributorModel> result = session.createQuery("from DistributorModel").list();
		return result;
	}

	@Override
	public void insert(DistributorModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public DistributorModel getById(String id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		DistributorModel result = session.get(DistributorModel.class, id);
		return result;
	}

	@Override
	public void update(DistributorModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(DistributorModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}
}
