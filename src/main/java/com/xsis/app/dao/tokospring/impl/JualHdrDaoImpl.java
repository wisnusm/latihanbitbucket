package com.xsis.app.dao.tokospring.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.app.tokospring.dao.JualHdrDao;
import com.xsis.app.tokospring.model.JualHdrModel;

@Repository
public class JualHdrDaoImpl implements JualHdrDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<JualHdrModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<JualHdrModel> result = session.createQuery("from JualHdrModel").list();
		return result;
	}

	@Override
	public void insert(JualHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public JualHdrModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		JualHdrModel result = session.get(JualHdrModel.class, id);
		return result;
	}

	@Override
	public void update(JualHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(JualHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}
}
