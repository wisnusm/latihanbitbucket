package com.xsis.app.dao.tokospring.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.app.tokospring.dao.ServisDetailDao;
import com.xsis.app.tokospring.model.ServisDetailModel;

@Repository
public class ServisDetailDaoImpl implements ServisDetailDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<ServisDetailModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<ServisDetailModel> result = session.createQuery("from ServisDetailModel").list();
		return result;
	}

	@Override
	public void insert(ServisDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public ServisDetailModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		ServisDetailModel result = session.get(ServisDetailModel.class, id);
		return result;
	}

	@Override
	public void update(ServisDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(ServisDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}
}
