package com.xsis.app.dao.tokospring.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.app.tokospring.dao.ServisHdrDao;
import com.xsis.app.tokospring.model.ServisHdrModel;

@Repository
public class ServisHdrDaoImpl implements ServisHdrDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ServisHdrModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<ServisHdrModel> result = session.createQuery("from ServisHdrModel").list();
		return result;
	}

	@Override
	public void insert(ServisHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
	}

	@Override
	public ServisHdrModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		ServisHdrModel result = session.get(ServisHdrModel.class, id);
		return result;
	}

	@Override
	public void update(ServisHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
	}

	@Override
	public void delete(ServisHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
	}

}
