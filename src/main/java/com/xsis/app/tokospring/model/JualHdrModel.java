package com.xsis.app.tokospring.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="TRX_JUAL_HDR")
public class JualHdrModel {
	
	private Integer id_jual_hdr;
	private Date tgl_pembelian;
	private String nama_pembeli;
	private String telpon;
	private Integer total_jual;
	private String tipe_bayar;
	private Integer bayar;
	private Integer kembalian;
	
	//1-M KEJUALDETAIL
	private List<JualDetailModel> jualdetail;
	
	
	@Id
	@GeneratedValue
	@Column(name="ID_JUAL_HDR")
	public Integer getId_jual_hdr() {
		return id_jual_hdr;
	}
	public void setId_jual_hdr(Integer id_jual_hdr) {
		this.id_jual_hdr = id_jual_hdr;
	}
	
	@Column(name="TANGGAL_PEMBELIAN")
	public Date getTgl_pembelian() {
		return tgl_pembelian;
	}
	public void setTgl_pembelian(Date tgl_pembelian) {
		this.tgl_pembelian = tgl_pembelian;
	}
	
	@Column(name="NAMA_PEMBELI")
	public String getNama_pembeli() {
		return nama_pembeli;
	}
	public void setNama_pembeli(String nama_pembeli) {
		this.nama_pembeli = nama_pembeli;
	}
	
	@Column(name="TELPON")
	public String getTelpon() {
		return telpon;
	}
	public void setTelpon(String telpon) {
		this.telpon = telpon;
	}
	
	@Column(name="TOTAL")
	public Integer getTotal_jual() {
		return total_jual;
	}
	public void setTotal_jual(Integer total_jual) {
		this.total_jual = total_jual;
	}
	
	@Column(name="TIPE_BAYAR")
	public String getTipe_bayar() {
		return tipe_bayar;
	}
	public void setTipe_bayar(String tipe_bayar) {
		this.tipe_bayar = tipe_bayar;
	}
	
	@Column(name="BAYAR")
	public Integer getBayar() {
		return bayar;
	}
	public void setBayar(Integer bayar) {
		this.bayar = bayar;
	}
	
	@Column(name="KEMBALIAN")
	public Integer getKembalian() {
		return kembalian;
	}
	public void setKembalian(Integer kembalian) {
		this.kembalian = kembalian;
	}
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="jualhdr")
	public List<JualDetailModel> getJualdetail() {
		return jualdetail;
	}
	public void setJualdetail(List<JualDetailModel> jualdetail) {
		this.jualdetail = jualdetail;
	}
	
	
}
