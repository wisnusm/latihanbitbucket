package com.xsis.app.tokospring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ITEM")
public class ItemModel {

	private String id_item;
	private String nama_produk;
	private String nama_item;
	private Integer harga_jual;
	private Integer stok;
	
	
	@Id
	@Column(name="ID_ITEM")
	public String getId_item() {
		return id_item;
	}
	public void setId_item(String id_item) {
		this.id_item = id_item;
	}
	
	@Column(name="NAMA_ITEM")
	public String getNama_item() {
		return nama_item;
	}
	public void setNama_item(String nama_item) {
		this.nama_item = nama_item;
	}
	@Column(name="NAMA_PRODUK")
	public String getNama_produk() {
		return nama_produk;
	}
	public void setNama_produk(String nama_produk) {
		this.nama_produk = nama_produk;
	}
	@Column(name="HARGA_JUAL")
	public Integer getHarga_jual() {
		return harga_jual;
	}
	public void setHarga_jual(Integer harga_jual) {
		this.harga_jual = harga_jual;
	}
	
	@Column(name="STOK")
	public Integer getStok() {
		return stok;
	}
	public void setStok(Integer stok) {
		this.stok = stok;
	}
}
