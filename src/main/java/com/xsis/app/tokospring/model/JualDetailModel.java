package com.xsis.app.tokospring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TRX_JUAL_DETAIL")
public class JualDetailModel {
	
	private Integer id_jual_detail;
	private String nama_produk;
	private String nama_item;
	private Integer harga_jual;
	private Integer jumlah_stok;
	private Integer sub_total;
	
	//M-1 KE ITEM
	private ItemModel item;
	private String id_item;
	
	//M-1 KE JUALHEADER
	private JualHdrModel jualhdr;
	private Integer id_jual_hdr;
	
	@Id
	@GeneratedValue
	@Column(name="ID_JUAL_DETAIL")
	public Integer getId_jual_detail() {
		return id_jual_detail;
	}
	public void setId_jual_detail(Integer id_jual_detail) {
		this.id_jual_detail = id_jual_detail;
	}
	
	@Column(name="NAMA_PRODUK")
	public String getNama_produk() {
		return nama_produk;
	}
	public void setNama_produk(String nama_produk) {
		this.nama_produk = nama_produk;
	}
	
	@Column(name="NAMA_ITEM")
	public String getNama_item() {
		return nama_item;
	}
	public void setNama_item(String nama_item) {
		this.nama_item = nama_item;
	}
	
	@Column(name="HARGA_JUAL")
	public Integer getHarga_jual() {
		return harga_jual;
	}
	public void setHarga_jual(Integer harga_jual) {
		this.harga_jual = harga_jual;
	}
	
	@Column(name="JUMLAH_STOK")
	public Integer getJumlah_stok() {
		return jumlah_stok;
	}
	public void setJumlah_stok(Integer jumlah_stok) {
		this.jumlah_stok = jumlah_stok;
	}
	
	@Column(name="SUB_TOTAL")
	public Integer getSub_total() {
		return sub_total;
	}
	public void setSub_total(Integer sub_total) {
		this.sub_total = sub_total;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_ITEM", nullable=false, updatable=false, insertable=false)
	public ItemModel getItem() {
		return item;
	}
	public void setItem(ItemModel item) {
		this.item = item;
	}
	
	@Column(name="ID_ITEM")
	public String getId_item() {
		return id_item;
	}
	public void setId_item(String id_item) {
		this.id_item = id_item;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_JUAL_HDR", nullable=false, updatable=false, insertable=false)
	public JualHdrModel getJualhdr() {
		return jualhdr;
	}
	public void setJualhdr(JualHdrModel jualhdr) {
		this.jualhdr = jualhdr;
	}
	
	@Column(name="ID_JUAL_HDR")
	public Integer getId_jual_hdr() {
		return id_jual_hdr;
	}
	public void setId_jual_hdr(Integer id_jual_hdr) {
		this.id_jual_hdr = id_jual_hdr;
	}
	
}
