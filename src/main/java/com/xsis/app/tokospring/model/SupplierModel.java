package com.xsis.app.tokospring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SUPPLIER")
public class SupplierModel {

	
	private String id_supplier;
	private String nama_supplier;
	private String alamat;
	private String nomor_hp;
	private String email;
	
	@Id
	@Column(name="ID_SUPPLIER")
	public String getId_supplier() {
		return id_supplier;
	}
	public void setId_supplier(String id_supplier) {
		this.id_supplier = id_supplier;
	}
	
	@Column(name="NAMA_SUPPLIER")
	public String getNama_supplier() {
		return nama_supplier;
	}
	public void setNama_supplier(String nama_supplier) {
		this.nama_supplier = nama_supplier;
	}
	
	@Column(name="ALAMAT")
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	@Column(name="NOMOR_HP")
	public String getNomor_hp() {
		return nomor_hp;
	}
	public void setNomor_hp(String nomor_hp) {
		this.nomor_hp = nomor_hp;
	}
	
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
