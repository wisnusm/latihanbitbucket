package com.xsis.app.tokospring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DISTRIBUTOR")
public class DistributorModel {
	
	private String id_distributor;
	private String nama_distributor;
	private String telpon_klaim;
	private String telpon_sc;
	private String alamat_klaim;
	private String alamat_sc;
	
	@Id
	@Column(name="ID_DISTRIBUTOR", unique=true)
	public String getId_distributor() {
		return id_distributor;
	}
	public void setId_distributor(String id_distributor) {
		this.id_distributor = id_distributor;
	}
	
	@Column(name="NAMA_DISTRIBUTOR", unique=true)
	public String getNama_distributor() {
		return nama_distributor;
	}
	public void setNama_distributor(String nama_distributor) {
		this.nama_distributor = nama_distributor;
	}
	
	@Column(name="TELEPON_KLAIM")
	public String getTelpon_klaim() {
		return telpon_klaim;
	}
	public void setTelpon_klaim(String telpon_klaim) {
		this.telpon_klaim = telpon_klaim;
	}
	
	@Column(name="TELEPON_SC")
	public String getTelpon_sc() {
		return telpon_sc;
	}
	public void setTelpon_sc(String telpon_sc) {
		this.telpon_sc = telpon_sc;
	}
	
	@Column(name="ALAMAT_KLAIM")
	public String getAlamat_klaim() {
		return alamat_klaim;
	}
	public void setAlamat_klaim(String alamat_klaim) {
		this.alamat_klaim = alamat_klaim;
	}
	
	@Column(name="ALAMAT_SC")
	public String getAlamat_sc() {
		return alamat_sc;
	}
	public void setAlamat_sc(String alamat_sc) {
		this.alamat_sc = alamat_sc;
	}
}
