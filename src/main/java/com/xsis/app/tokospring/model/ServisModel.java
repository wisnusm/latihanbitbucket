package com.xsis.app.tokospring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SERVIS")
public class ServisModel {
	
	private String id_servis;
	private String nama_servis;
	private Integer biaya_servis;
	private String keterangan;
	
	@Id
	@Column(name="ID_SERVIS", unique=true)
	public String getId_servis() {
		return id_servis;
	}
	public void setId_servis(String id_servis) {
		this.id_servis = id_servis;
	}
	
	@Column(name="NAMA_SERVIS", unique=true)
	public String getNama_servis() {
		return nama_servis;
	}
	public void setNama_servis(String nama_servis) {
		this.nama_servis = nama_servis;
	}
	
	@Column(name="BIAYA_SERVIS")
	public Integer getBiaya_servis() {
		return biaya_servis;
	}
	public void setBiaya_servis(Integer biaya_servis) {
		this.biaya_servis = biaya_servis;
	}
	
	@Column(name="KETERANGAN_SERVIS")
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}	
}	

