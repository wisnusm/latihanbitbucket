package com.xsis.app.tokospring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TRX_SERVIS_DETAIL")
public class ServisDetailModel {
	
	private Integer id_servis_detail;
	private String nama_servis;
	private String keterangan;
	private Integer biaya;
	private Integer jumlah_stok;
	private Integer sub_total;
	
	// M-1 KE SERVIS
	private ServisModel servis;
	private String id_servis;
	
	// M-1 KE SERVISHEADER
	private ServisHdrModel serHdr;
	private Integer id_servis_hdr;
	
	@Id
	@GeneratedValue
	@Column(name="ID_SERVIS_DETAIL")
	public Integer getId_servis_detail() {
		return id_servis_detail;
	}
	public void setId_servis_detail(Integer id_servis_detail) {
		this.id_servis_detail = id_servis_detail;
	}
	
	@Column(name="NAMA_SERVIS")
	public String getNama_servis() {
		return nama_servis;
	}
	public void setNama_servis(String nama_servis) {
		this.nama_servis = nama_servis;
	}
	
	@Column(name="KETERANGAN")
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	@Column(name="BIAYA")
	public Integer getBiaya() {
		return biaya;
	}
	public void setBiaya(Integer biaya) {
		this.biaya = biaya;
	}
	
	@Column(name="JUMLAH_STOK")
	public Integer getJumlah_stok() {
		return jumlah_stok;
	}
	public void setJumlah_stok(Integer jumlah_stok) {
		this.jumlah_stok = jumlah_stok;
	}
	
	@Column(name="SUB_TOTAL")
	public Integer getSub_total() {
		return sub_total;
	}
	public void setSub_total(Integer sub_total) {
		this.sub_total = sub_total;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_SERVIS", nullable=false, updatable=false, insertable=false)
	public ServisModel getServis() {
		return servis;
	}
	public void setServis(ServisModel servis) {
		this.servis = servis;
	}
	
	@Column(name="ID_SERVIS")
	public String getId_servis() {
		return id_servis;
	}
	public void setId_servis(String id_servis) {
		this.id_servis = id_servis;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_SERVIS_HDR", nullable=false, updatable=false, insertable=false)
	public ServisHdrModel getSerHdr() {
		return serHdr;
	}
	public void setSerHdr(ServisHdrModel serHdr) {
		this.serHdr = serHdr;
	}
	
	@Column(name="ID_SERVIS_HDR")
	public Integer getId_servis_hdr() {
		return id_servis_hdr;
	}
	public void setId_servis_hdr(Integer id_servis_hdr) {
		this.id_servis_hdr = id_servis_hdr;
	}	
}
