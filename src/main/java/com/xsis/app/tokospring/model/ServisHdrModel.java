package com.xsis.app.tokospring.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="TRX_SERVIS_HEADER")
public class ServisHdrModel {

	private Integer id_servis_hdr;
	private Date tgl_servis;
	private Date tgl_ambil_servis;
	private String pengaju_servis;
	private String telpon;
	private Integer total;
	private String tipe_bayar;
	private Integer bayar;
	private Integer kembalian;
	
	//1-M KE SERVISDETAIL
	private List<ServisDetailModel> listServisDetail;
	
	@Id
	@GeneratedValue
	@Column(name="ID_SERVIS_HDR")
	public Integer getId_servis_hdr() {
		return id_servis_hdr;
	}
	public void setId_servis_hdr(Integer id_servis_hdr) {
		this.id_servis_hdr = id_servis_hdr;
	}
	
	@Column(name="TGL_SERVIS")
	public Date getTgl_servis() {
		return tgl_servis;
	}
	public void setTgl_servis(Date tgl_servis) {
		this.tgl_servis = tgl_servis;
	}
	
	@Column(name="AMBIL_SERVIS")
	public Date getTgl_ambil_servis() {
		return tgl_ambil_servis;
	}
	public void setTgl_ambil_servis(Date tgl_ambil_servis) {
		this.tgl_ambil_servis = tgl_ambil_servis;
	}
	
	@Column(name="PENGAJUAN_SERVIS")
	public String getPengaju_servis() {
		return pengaju_servis;
	}
	public void setPengaju_servis(String pengaju_servis) {
		this.pengaju_servis = pengaju_servis;
	}
	
	@Column(name="TELPON")
	public String getTelpon() {
		return telpon;
	}
	public void setTelpon(String telpon) {
		this.telpon = telpon;
	}
	
	@Column(name="TOTAL")
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	@Column(name="TIPE_BAYAR")
	public String getTipe_bayar() {
		return tipe_bayar;
	}
	public void setTipe_bayar(String tipe_bayar) {
		this.tipe_bayar = tipe_bayar;
	}
	
	@Column(name="BAYAR")
	public Integer getBayar() {
		return bayar;
	}
	public void setBayar(Integer bayar) {
		this.bayar = bayar;
	}
	
	@Column(name="KEMBALIAN")
	public Integer getKembalian() {
		return kembalian;
	}
	public void setKembalian(Integer kembalian) {
		this.kembalian = kembalian;
	}
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="serHdr")
	public List<ServisDetailModel> getListServisDetail() {
		return listServisDetail;
	}
	public void setListServisDetail(List<ServisDetailModel> listServisDetail) {
		this.listServisDetail = listServisDetail;
	}
		
	
}
