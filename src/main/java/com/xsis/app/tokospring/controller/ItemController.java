package com.xsis.app.tokospring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.app.tokospring.model.ItemModel;
import com.xsis.app.tokospring.service.ItemService;

@Controller
public class ItemController {
private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private ItemService service;
	
	@RequestMapping(value="/item")
	public String index(Model model){
		return "item";
	}
	
	@RequestMapping(value="/item/list")
	public String list(Model model){
		List<ItemModel> items = null;		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		return "item/list";
	}
	
	@RequestMapping(value="/item/add")
	public String add(){
		return "item/add";
	}
	
	@RequestMapping(value="/item/save")
	public String simpan(Model model, @ModelAttribute ItemModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String id_item = request.getParameter("id_item");
		String result ="";
		try {
			if(proses.equals("insert")){
				ItemModel itemmod = null;
				itemmod = this.service.getById(id_item);
				if (itemmod ==  null) {
					this.service.insert(item);
					result="berhasil";
				} else {
					result ="gagal";
					model.addAttribute("message", "ID " + id_item + " Telah Terdaftar !, Silahkan Coba Lagi");
				}
			}else if(proses.equals("update")){
				result="berhasil";
				this.service.update(item);
			}else if(proses.equals("delete")){
				result="berhasil";
				this.service.delete(item);
			}			
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("result",result);		
		return "item/save";
	}
	
	@RequestMapping(value="/item/edit")
	public String edit(Model model, HttpServletRequest request){
		String id = request.getParameter("id_item");		
		ItemModel item = null;
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "item/edit";
	}
	
	@RequestMapping(value="/item/delete")
	public String delete(Model model, HttpServletRequest request){
		String id = request.getParameter("id_item");	
		ItemModel item = null;
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "item/delete";
	}
	
	@RequestMapping(value="/item/detail")
	public String detail(Model model, HttpServletRequest request){
		String id = request.getParameter("id_item");
		ItemModel item = null;
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "item/detail";
	}
}
