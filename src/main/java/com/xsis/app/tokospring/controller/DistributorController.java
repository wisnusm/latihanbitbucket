package com.xsis.app.tokospring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.app.tokospring.model.DistributorModel;
import com.xsis.app.tokospring.service.DistributorService;

@Controller
public class DistributorController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private DistributorService service;
	
	@RequestMapping(value="/distributor")
	public String index(Model model){
		return "distributor";
	}
	
	@RequestMapping(value="/distributor/list")
	public String list(Model model){
		List<DistributorModel> items = null;		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		return "distributor/list";
	}
	
	@RequestMapping(value="/distributor/add")
	public String add(){
		return "distributor/add";
	}
	
	@RequestMapping(value="/distributor/save")
	public String simpan(Model model, @ModelAttribute DistributorModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result ="";
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}		
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);		
		return "distributor/save";
	}
	
	@RequestMapping(value="/distributor/edit")
	public String edit(Model model, HttpServletRequest request){
		String id = request.getParameter("id_distributor");		
		DistributorModel item = null;
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "distributor/edit";
	}
	
	@RequestMapping(value="/distributor/delete")
	public String delete(Model model, HttpServletRequest request){
		String id = request.getParameter("id_distributor");	
		DistributorModel item = null;
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "distributor/delete";
	}
	
	@RequestMapping(value="/distributor/detail")
	public String detail(Model model, HttpServletRequest request){
		String id = request.getParameter("id_distributor");
		DistributorModel item = null;
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "distributor/detail";
	}
}
