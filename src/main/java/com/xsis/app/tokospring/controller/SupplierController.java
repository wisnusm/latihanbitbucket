package com.xsis.app.tokospring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.app.tokospring.model.SupplierModel;
import com.xsis.app.tokospring.service.SupplierService;

@Controller
public class SupplierController {
private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private SupplierService service;
	
	@RequestMapping(value="/supplier")
	public String index(Model model){
		
		return "supplier";
	}
	
	@RequestMapping(value="/supplier/list")
	public String list(Model model){
		// membuat object list dari class Fakultas model
		List<SupplierModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "supplier/list";
	}
	
	@RequestMapping(value="/supplier/add")
	public String add(){
		return "supplier/add";
	}
	
	@RequestMapping(value="/supplier/save")
	public String save(Model model, @ModelAttribute SupplierModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String id_supplier = request.getParameter("id_supplier");
		String result ="";
		// proses input ke database
		try {
			if(proses.equals("insert")){
				SupplierModel supplier = null;
				supplier = this.service.getById(id_supplier);
				if (supplier ==  null) {
					this.service.insert(item);
					result="berhasil";
				} else {
					result ="gagal";
					model.addAttribute("message", "ID " + id_supplier + " Telah Terdaftar !, Silahkan Coba Lagi");
				}
			}else if(proses.equals("update")){
				result="berhasil";
				this.service.update(item);
			}else if(proses.equals("delete")){
				result="berhasil";
				this.service.delete(item);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("result",result);
		
		return "supplier/save";
	}
	
	@RequestMapping(value="/supplier/edit")
	public String edit(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		String id_supplier = request.getParameter("id_supplier");
		
		// siapkan object fakultas model
		SupplierModel item = null;
		// request ke database
		try {
			item = this.service.getById(id_supplier);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "supplier/edit";
	}
	
	@RequestMapping(value="/supplier/detail")
	public String detail(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		String id = request.getParameter("id_supplier");
		
		// siapkan object fakultas model
		SupplierModel item = null;
		// request ke database
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable item kemudian diisi dengan object item
		model.addAttribute("item", item);
		return "supplier/detail";
	}
	
	@RequestMapping(value="/supplier/delete")
	public String delete(Model model, HttpServletRequest request){
		// menangkap paremeter yang dikirim dari view
		String id_supplier = request.getParameter("id_supplier");
		
		// siapkan object fakultas model
		SupplierModel item = null;
		// request ke database
		try {
			item = this.service.getById(id_supplier);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "supplier/delete";
	}
}
