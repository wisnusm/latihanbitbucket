package com.xsis.app.tokospring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.app.tokospring.model.ServisHdrModel;
import com.xsis.app.tokospring.service.ServisHdrService;

@Controller
public class ServisDetailController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private ServisHdrService servisServiceHdr;
	
	@RequestMapping(value="/servis_detail")
	public String index(Model model){
		
		return "servis_detail";
	}
	
	@RequestMapping(value="/servis_detail/list")
	public String list(Model model){
		List<ServisHdrModel> items = null;		
		try {
			items = this.servisServiceHdr.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		return "servis_detail/list";
	}
	
	@RequestMapping(value="/servis_detail/detail")
	public String detail(Model model, HttpServletRequest request){
		int id = Integer.valueOf(request.getParameter("id_servis_hdr"));
		ServisHdrModel item = null;
		try {
			item = this.servisServiceHdr.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "servis_detail/detail";
	}
}
