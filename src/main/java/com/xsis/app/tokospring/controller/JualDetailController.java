package com.xsis.app.tokospring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.app.tokospring.model.JualHdrModel;
import com.xsis.app.tokospring.service.JualHdrService;

@Controller
public class JualDetailController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private JualHdrService jualServiceHdr;
	
	@RequestMapping(value="/penjualan_detail")
	public String index(Model model){
		
		return "penjualan_detail";
	}
	
	@RequestMapping(value="/penjualan_detail/list")
	public String list(Model model){
		List<JualHdrModel> items = null;		
		try {
			items = this.jualServiceHdr.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		return "penjualan_detail/list";
	}
	
	@RequestMapping(value="/penjualan_detail/detail")
	public String detail(Model model, HttpServletRequest request){
		int id = Integer.valueOf(request.getParameter("id_jual_hdr"));
		JualHdrModel item = null;
		try {
			item = this.jualServiceHdr.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "penjualan_detail/detail";
	}
}
