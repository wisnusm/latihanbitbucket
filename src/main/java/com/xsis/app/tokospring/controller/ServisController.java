package com.xsis.app.tokospring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import com.xsis.app.tokospring.model.ServisModel;
import com.xsis.app.tokospring.service.ServisService;

@Controller
public class ServisController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private ServisService service;
	
	@RequestMapping(value="/servis")
	public String index(Model model){	
		return "servis";
	}
	
	@RequestMapping(value="/servis/list")
	public String list(Model model){
		List<ServisModel> items = null;		
		try {
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", items);
		return "servis/list";
	}
	
	@RequestMapping(value="/servis/add")
	public String add(){
		return "servis/add";
	}
	
	@RequestMapping(value="/servis/save")
	public String simpan(Model model, @ModelAttribute ServisModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result ="";
		try {
			if(proses.equals("insert")){
				this.service.insert(item);					
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){		
				this.service.delete(item);
			}		
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);		
		return "servis/save";
	}
	
	@RequestMapping(value="/servis/edit")
	public String edit(Model model, HttpServletRequest request){
		String id = request.getParameter("id_servis");		
		ServisModel item = null;
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "servis/edit";
	}
	
	@RequestMapping(value="/servis/delete")
	public String delete(Model model, HttpServletRequest request){
		String id = request.getParameter("id_servis");		
		ServisModel item = null;
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "servis/delete";
	}
	
	@RequestMapping(value="/servis/detail")
	public String detail(Model model, HttpServletRequest request){
		String id = request.getParameter("id_servis");	
		ServisModel item = null;
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", item);
		return "servis/detail";
	}
}
