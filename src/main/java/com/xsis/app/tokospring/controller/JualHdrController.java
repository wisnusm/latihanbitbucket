package com.xsis.app.tokospring.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.xsis.app.tokospring.model.ItemModel;
import com.xsis.app.tokospring.model.JualDetailModel;
import com.xsis.app.tokospring.model.JualHdrModel;
import com.xsis.app.tokospring.service.ItemService;
import com.xsis.app.tokospring.service.JualDetailService;
import com.xsis.app.tokospring.service.JualHdrService;

@Controller
public class JualHdrController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private JualHdrService jualHdrService;
	
	@Autowired
	private JualDetailService jualDetailService;
	
	List<JualDetailModel> jualDetailList = null;
	
	@SuppressWarnings("unused")
	@Autowired
	private HttpSession session;
	
	@RequestMapping(value="/penjualan_header")
	public String index(Model model){
		jualDetailList = new ArrayList<JualDetailModel>();
		return "penjualan_header";
	}
	
	@RequestMapping(value="/penjualan_header/add")
	public String add(Model model, HttpServletRequest request){	
		List<ItemModel> listItem = null;
		String idExcludeList = "";
		try {
			idExcludeList = this.getIdExcludeList(jualDetailList);
			listItem = this.itemService.getByIdNotIn(idExcludeList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("listItem", listItem);
		return "penjualan_header/add";
	}
	
	// ADD DETAIL
	@RequestMapping(value="/penjualan_header/pilih_detail")
	public String pilih_detail(Model model, HttpServletRequest request,@RequestParam(value="itemPilih") String[] itemIdPilih) throws Exception{
		
		for (int i = 0; i < itemIdPilih.length; i++) {
			itemIdPilih[i] =itemIdPilih[i].replace("[", "").replace("]", "").replace("\"", "");
			
			ItemModel im = new ItemModel();
			im = this.itemService.getById(itemIdPilih[i]);
			
			JualDetailModel jdm = new JualDetailModel();
			jdm.setId_item(im.getId_item());
			jdm.setItem(im);
			jdm.setHarga_jual(im.getHarga_jual());
			
			jualDetailList.add(jdm);
		}
		 model.addAttribute("jualDetailList", jualDetailList);
		 model.addAttribute("success", "success");
		
		
		return "penjualan_header";
	}
	
	// HAPUS DATA DARI LIST
	@RequestMapping(value = "/penjualan_header/hapus")
	public String Hapus(Model model, HttpServletRequest request) {

		String id = request.getParameter("idx");
		if (jualDetailList.size() > 0) {
			for (int i = 0; i < jualDetailList.size(); i++) {
				String idservis = jualDetailList.get(i).getId_item();
				if (id.equals(idservis)) {
					jualDetailList.remove(i);
				}
			}
		}

		model.addAttribute("jualDetailList", jualDetailList);
		model.addAttribute("success", "success");
		return "servis_header";
	}
	
	// UNTUK KETIKA DIPILIH, DATA DI ADD ILANG
	public String getIdExcludeList(List<JualDetailModel> jualDetailList) {
		String idExcludeList = "";
		String comma = "";
		if (jualDetailList.size() > 0) {
			for (int i = 0; i < jualDetailList.size(); i++) {
				if (i > 0) {
					comma = ",";
				} else {
					comma = " ";
				}

				String id = jualDetailList.get(i).getId_item();
				idExcludeList = idExcludeList + comma + "'"+id+"'";
			}
		}

		return idExcludeList;
	}
	
	@RequestMapping(value="/penjualan/simpan")
	public String cetak(Model model, HttpServletRequest request) throws Exception {
		
		//Get data dari form header
		String nama_pembeli = request.getParameter("nama_pembeli");
		String telpon = request.getParameter("telpon");
		String total_jual = request.getParameter("total_jual");
		String tipe_bayar = request.getParameter("tipe_bayar");
		String bayar = request.getParameter("bayar");
		String kembalian = request.getParameter("kembalian");
		
		String tgl_pembelianStr = request.getParameter("tgl_pembelianStr");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date tgl_pembelian = formatter.parse(tgl_pembelianStr);
		
		//Set data dari form header ke model header
		JualHdrModel jhm = new JualHdrModel();
		jhm.setNama_pembeli(nama_pembeli);
		jhm.setTelpon(telpon);
		jhm.setTotal_jual(Integer.valueOf(total_jual));
		jhm.setTipe_bayar(tipe_bayar);
		jhm.setBayar(Integer.valueOf(bayar));
		jhm.setKembalian(Integer.valueOf(kembalian));
		jhm.setTgl_pembelian(tgl_pembelian);
		
		
		try {
			//simpan beli header
			this.jualHdrService.insert(jhm);
			int headerID = jhm.getId_jual_hdr();
			
			String jumlahDetail = request.getParameter("jumlahDetail");
			int jumlahDetailInteger = Integer.valueOf(jumlahDetail);
			
			for (int i = 0; i < jumlahDetailInteger; i++) {
				//Set per detil satu satu
				String idItemDetail = request.getParameter("idItemDetail_"+i);
				ItemModel item = new ItemModel();
				item = this.itemService.getById(idItemDetail);
				
				String namaProdukDetail = request.getParameter("namaProdukDetail_"+i);
				String namaItemDetail = request.getParameter("namaItemDetail_"+i);
				String hargajualDetail = request.getParameter("hargajualDetail_"+i);
				String qtyDetail = request.getParameter("qtyDetail_"+i);
				String hasilDetail = request.getParameter("hasilDetail_"+i);
				
				
				
				JualDetailModel jdm = new JualDetailModel();
				jdm.setId_item(idItemDetail);
				jdm.setItem(item);
				jdm.setId_jual_hdr(headerID);
				jdm.setNama_produk(namaProdukDetail);
				jdm.setNama_item(namaItemDetail);
				jdm.setHarga_jual(Integer.valueOf(hargajualDetail));
				jdm.setJumlah_stok(Integer.valueOf(qtyDetail));
				jdm.setSub_total(Integer.valueOf(hasilDetail));
				
				//Simpan per detil satu satu
				this.jualDetailService.insert(jdm);
				
				//bagian mengurangi item
				int qty_now = item.getStok();
				int qty_input = Integer.valueOf(qtyDetail);
				int qty_baru = qty_now - qty_input;
				item.setStok(qty_baru);
				//set item dengan qty baru
				this.itemService.update(item);
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "penjualan_header";
	}
}
