package com.xsis.app.tokospring.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.xsis.app.tokospring.model.ServisDetailModel;
import com.xsis.app.tokospring.model.ServisHdrModel;
import com.xsis.app.tokospring.model.ServisModel;
import com.xsis.app.tokospring.service.ServisDetailService;
import com.xsis.app.tokospring.service.ServisHdrService;
import com.xsis.app.tokospring.service.ServisService;

@Controller
public class ServisHdrController {
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private ServisHdrService serHdrService;
	
	@Autowired
	private ServisDetailService serDtlService; 
	
	@Autowired
	private ServisService serService;
	
	List<ServisDetailModel> servisDetailList = null;
	
	@SuppressWarnings("unused")
	@Autowired
	private HttpSession session;
	
	@RequestMapping(value="/servis_header")
	public String index(Model model){
		servisDetailList = new ArrayList<ServisDetailModel>();
		return "servis_header";
	}
	
	@RequestMapping(value="/servis_header/add")
	public String add(Model model, HttpServletRequest request){	
		List<ServisModel> listServis = null;
		String idExcludeList = "";
		try {
			idExcludeList = this.getIdExcludeList(servisDetailList);
			listServis = this.serService.getByIdNotIn(idExcludeList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("listServis", listServis);
		return "servis_header/add";
	}
	
	@RequestMapping(value="/servis_header/pilih_detail")
	public String pilih_detail(Model model, HttpServletRequest request,@RequestParam(value="itemPilih") String[] itemIdPilih) throws Exception{
		
		for (int i = 0; i < itemIdPilih.length; i++) {
			itemIdPilih[i] =itemIdPilih[i].replace("[", "").replace("]", "").replace("\"", "");
			
			ServisModel im = new ServisModel();
			im = this.serService.getById(itemIdPilih[i]);
			
			ServisDetailModel jdm = new ServisDetailModel();
			jdm.setId_servis(im.getId_servis());
			jdm.setServis(im);
			jdm.setBiaya(im.getBiaya_servis());

			
			servisDetailList.add(jdm);
		}
		 model.addAttribute("servisDetailList", servisDetailList);
		 model.addAttribute("success", "success");
		
		
		return "servis_header";
	}
	
	// Hapus datta di list
	@RequestMapping(value = "/servis_header/hapus")
	public String Hapus(Model model, HttpServletRequest request) {

		String id = request.getParameter("idx");
		if (servisDetailList.size() > 0) {
			for (int i = 0; i < servisDetailList.size(); i++) {
				String idservis = servisDetailList.get(i).getId_servis();
				if (id.equals(idservis)) {
					servisDetailList.remove(i);
				}
			}
		}

		model.addAttribute("servisDetailList", servisDetailList);
		model.addAttribute("success", "success");
		return "servis_header";
	}
	
	public String getIdExcludeList(List<ServisDetailModel> servisDetailList) {
		String idExcludeList = "";
		String comma = "";
		if (servisDetailList.size() > 0) {
			for (int i = 0; i < servisDetailList.size(); i++) {
				if (i > 0) {
					comma = ",";
				} else {
					comma = " ";
				}

				String id = servisDetailList.get(i).getId_servis();
				idExcludeList = idExcludeList + comma + "'"+id+"'";
			}
		}

		return idExcludeList;
	}
	
	// Save Data Servis Header
	@RequestMapping(value="/servis_header/simpan")
	public String cetak(Model model, HttpServletRequest request) throws Exception {
		
		//Get data dari form header
		String pengaju_servis = request.getParameter("pengaju_servis");
		String telpon = request.getParameter("telpon");
		String total_jual = request.getParameter("total_jual");
		String tipe_bayar = request.getParameter("tipe_bayar");
		String bayar = request.getParameter("bayar");
		String kembalian = request.getParameter("kembalian");
		
		String tgl_servisStr = request.getParameter("tgl_servisStr");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date tgl_servis = formatter.parse(tgl_servisStr);
		
		String tgl_ambil_servisStr = request.getParameter("tgl_ambil_servisStr");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
		Date tgl_ambil_servis = formatter2.parse(tgl_ambil_servisStr);
		
		//Set data dari form header ke model header
		ServisHdrModel ser = new ServisHdrModel();
		ser.setPengaju_servis(pengaju_servis);
		ser.setTelpon(telpon);
		ser.setTotal(Integer.valueOf(total_jual));
		ser.setTipe_bayar(tipe_bayar);
		ser.setBayar(Integer.valueOf(bayar));
		ser.setKembalian(Integer.valueOf(kembalian));
		ser.setTgl_servis(tgl_servis);
		ser.setTgl_ambil_servis(tgl_ambil_servis);
		
		
		try {
			//simpan beli header
			this.serHdrService.insert(ser);
			int headerID = ser.getId_servis_hdr();
			
			String jumlahDetail = request.getParameter("jumlahDetail");
			int jumlahDetailInteger = Integer.valueOf(jumlahDetail);
			
			for (int i = 0; i < jumlahDetailInteger; i++) {
				String idServisDetail = request.getParameter("idServisDetail_"+i);
				ServisModel servis = new ServisModel();
				servis = this.serService.getById(idServisDetail);
				
				String namaServisDetail = request.getParameter("namaServisDetail_"+i);
				String keteranganDetail = request.getParameter("keteranganDetail_"+i);
				String hargajualDetail = request.getParameter("hargajualDetail_"+i);
				String qtyDetail = request.getParameter("qtyDetail_"+i);
				String hasilDetail = request.getParameter("hasilDetail_"+i);
				
				ServisDetailModel sdm = new ServisDetailModel();
				sdm.setId_servis(idServisDetail);
				sdm.setServis(servis);
				sdm.setId_servis_hdr(headerID);
				sdm.setNama_servis(namaServisDetail);
				sdm.setKeterangan(keteranganDetail);
				sdm.setBiaya(Integer.valueOf(hargajualDetail));
				sdm.setJumlah_stok(Integer.valueOf(qtyDetail));
				sdm.setSub_total(Integer.valueOf(hasilDetail));
				
				this.serDtlService.insert(sdm);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "servis_header";
	}
}
