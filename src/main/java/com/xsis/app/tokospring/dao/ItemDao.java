package com.xsis.app.tokospring.dao;

import java.util.List;

import com.xsis.app.tokospring.model.ItemModel;


public interface ItemDao {
	public List<ItemModel> get() throws Exception;
	public void insert(ItemModel model) throws Exception;
	public ItemModel getById(String id) throws Exception;
	public void update(ItemModel model) throws Exception;
	public void delete(ItemModel model) throws Exception;
	public List<ItemModel> getByIdNotIn(String idExcludeList) throws Exception;
}
