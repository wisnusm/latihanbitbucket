package com.xsis.app.tokospring.dao;

import java.util.List;

import com.xsis.app.tokospring.model.DistributorModel;

public interface DistributorDao {
	public List<DistributorModel> get() throws Exception;
	public void insert(DistributorModel model) throws Exception;
	public DistributorModel getById(String id) throws Exception;
	public void update(DistributorModel model) throws Exception;
	public void delete(DistributorModel model) throws Exception;
}
