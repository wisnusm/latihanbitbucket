package com.xsis.app.tokospring.dao;

import java.util.List;

import com.xsis.app.tokospring.model.JualHdrModel;

public interface JualHdrDao {
	public List<JualHdrModel> get() throws Exception;
	public void insert(JualHdrModel model) throws Exception;
	public JualHdrModel getById(int id) throws Exception;
	public void update(JualHdrModel model) throws Exception;
	public void delete(JualHdrModel model) throws Exception;
}
