package com.xsis.app.tokospring.dao;

import java.util.List;

import com.xsis.app.tokospring.model.ServisDetailModel;

public interface ServisDetailDao {
	public List<ServisDetailModel> get() throws Exception;
	public void insert(ServisDetailModel model) throws Exception;
	public ServisDetailModel getById(int id) throws Exception;
	public void update(ServisDetailModel model) throws Exception;
	public void delete(ServisDetailModel model) throws Exception;
}
