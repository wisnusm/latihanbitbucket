package com.xsis.app.tokospring.dao;

import java.util.List;

import com.xsis.app.tokospring.model.SupplierModel;

public interface SupplierDao {
	public List<SupplierModel> get() throws Exception;
	public void insert(SupplierModel model) throws Exception;
	public void update(SupplierModel model) throws Exception;
	public void delete(SupplierModel model) throws Exception;
	public SupplierModel getById(String id_supplier) throws Exception;	
}
