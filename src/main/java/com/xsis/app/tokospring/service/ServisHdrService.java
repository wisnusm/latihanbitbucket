package com.xsis.app.tokospring.service;

import java.util.List;

import com.xsis.app.tokospring.model.ServisHdrModel;

public interface ServisHdrService {
	public List<ServisHdrModel> get() throws Exception;
	public void insert(ServisHdrModel model) throws Exception;
	public ServisHdrModel getById(int id) throws Exception;
	public void update(ServisHdrModel model) throws Exception;
	public void delete(ServisHdrModel model) throws Exception;
}
