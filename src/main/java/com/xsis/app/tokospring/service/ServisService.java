package com.xsis.app.tokospring.service;

import java.util.List;

import com.xsis.app.tokospring.model.ServisModel;

public interface ServisService {
	public List<ServisModel> get() throws Exception;
	public void insert(ServisModel model) throws Exception;
	public ServisModel getById(String itemIdPilih) throws Exception;
	public void update(ServisModel model) throws Exception;
	public void delete(ServisModel model) throws Exception;
	public List<ServisModel> getByIdNotIn(String idExcludeList) throws Exception;
}
