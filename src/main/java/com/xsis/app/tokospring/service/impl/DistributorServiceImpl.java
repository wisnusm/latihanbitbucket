package com.xsis.app.tokospring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.app.tokospring.dao.DistributorDao;
import com.xsis.app.tokospring.model.DistributorModel;
import com.xsis.app.tokospring.service.DistributorService;

@Service
@Transactional
public class DistributorServiceImpl implements DistributorService {
	
	@Autowired
	private DistributorDao dao;

	@Override
	public List<DistributorModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(DistributorModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public DistributorModel getById(String id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(DistributorModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(DistributorModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}
	
}
