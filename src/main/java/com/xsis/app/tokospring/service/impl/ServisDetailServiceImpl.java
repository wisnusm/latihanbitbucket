package com.xsis.app.tokospring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.app.tokospring.dao.ServisDetailDao;
import com.xsis.app.tokospring.model.ServisDetailModel;
import com.xsis.app.tokospring.service.ServisDetailService;

@Service
@Transactional
public class ServisDetailServiceImpl implements ServisDetailService {
	
	@Autowired
	private ServisDetailDao dao;
	
	@Override
	public List<ServisDetailModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(ServisDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public ServisDetailModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(ServisDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(ServisDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

}
