package com.xsis.app.tokospring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.app.tokospring.dao.JualDetailDao;
import com.xsis.app.tokospring.model.JualDetailModel;
import com.xsis.app.tokospring.service.JualDetailService;

@Service
@Transactional
public class JualDetailServiceImpl implements JualDetailService {

	@Autowired
	private JualDetailDao dao;
	
	@Override
	public List<JualDetailModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(JualDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public JualDetailModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(JualDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(JualDetailModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

}
