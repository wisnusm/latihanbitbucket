package com.xsis.app.tokospring.service;

import java.util.List;

import com.xsis.app.tokospring.model.DistributorModel;


public interface DistributorService {
	public List<DistributorModel> get() throws Exception;
	public void insert(DistributorModel model) throws Exception;
	public DistributorModel getById(String id) throws Exception;
	public void update(DistributorModel model) throws Exception;
	public void delete(DistributorModel model) throws Exception;
}
