package com.xsis.app.tokospring.service;

import java.util.List;

import com.xsis.app.tokospring.model.JualDetailModel;

public interface JualDetailService {
	public List<JualDetailModel> get() throws Exception;
	public void insert(JualDetailModel model) throws Exception;
	public JualDetailModel getById(int id) throws Exception;
	public void update(JualDetailModel model) throws Exception;
	public void delete(JualDetailModel model) throws Exception;
}
