package com.xsis.app.tokospring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.app.tokospring.dao.JualHdrDao;
import com.xsis.app.tokospring.model.JualHdrModel;
import com.xsis.app.tokospring.service.JualHdrService;

@Service
@Transactional
public class JualHdrServiceImpl implements JualHdrService {
	
	@Autowired
	private JualHdrDao dao;

	@Override
	public List<JualHdrModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(JualHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public JualHdrModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(JualHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(JualHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}
}
