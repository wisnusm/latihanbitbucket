package com.xsis.app.tokospring.service;

import java.util.List;

import com.xsis.app.tokospring.model.ServisDetailModel;

public interface ServisDetailService {
	public List<ServisDetailModel> get() throws Exception;
	public void insert(ServisDetailModel model) throws Exception;
	public ServisDetailModel getById(int id) throws Exception;
	public void update(ServisDetailModel model) throws Exception;
	public void delete(ServisDetailModel model) throws Exception;
}
