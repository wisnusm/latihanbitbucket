package com.xsis.app.tokospring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.app.tokospring.dao.ServisHdrDao;
import com.xsis.app.tokospring.model.ServisHdrModel;
import com.xsis.app.tokospring.service.ServisHdrService;

@Service
@Transactional
public class ServisHdrServiceImpl implements ServisHdrService {

	@Autowired
	private ServisHdrDao dao;

	@Override
	public List<ServisHdrModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(ServisHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public ServisHdrModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void update(ServisHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(ServisHdrModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}	
}
