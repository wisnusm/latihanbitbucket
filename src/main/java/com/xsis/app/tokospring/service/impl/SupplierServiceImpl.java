package com.xsis.app.tokospring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.app.tokospring.dao.SupplierDao;
import com.xsis.app.tokospring.model.SupplierModel;
import com.xsis.app.tokospring.service.SupplierService;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService{
	
	@Autowired
	private SupplierDao dao;
	
	@Override
	public List<SupplierModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(SupplierModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public void update(SupplierModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(SupplierModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

	@Override
	public SupplierModel getById(String id_supplier) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id_supplier);
	}

}
