package com.xsis.app.tokospring.service;

import java.util.List;

import com.xsis.app.tokospring.model.JualHdrModel;

public interface JualHdrService {
	public List<JualHdrModel> get() throws Exception;
	public void insert(JualHdrModel model) throws Exception;
	public JualHdrModel getById(int id) throws Exception;
	public void update(JualHdrModel model) throws Exception;
	public void delete(JualHdrModel model) throws Exception;
}
